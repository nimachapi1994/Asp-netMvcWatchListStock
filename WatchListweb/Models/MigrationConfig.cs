﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity.Migrations;
namespace WatchListweb.Models
{
    internal sealed class MigrationConfig:DbMigrationsConfiguration<model>
    {
        public MigrationConfig()
        {
            AutomaticMigrationDataLossAllowed = true;
            AutomaticMigrationsEnabled = true;
            ContextKey = "WatchList.Models.model";
        }
    }
}