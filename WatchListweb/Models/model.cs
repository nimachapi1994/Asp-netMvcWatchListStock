namespace WatchListweb.Models
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class model : DbContext
    {
     
        static model()
        {
            Database.SetInitializer<model>(new MigrateDatabaseToLatestVersion<model,MigrationConfig>(true));
        }
        public model()
            : base("name=model")
        {
        }

      public virtual DbSet<Add> Adds { get; set; }
    }

    
}