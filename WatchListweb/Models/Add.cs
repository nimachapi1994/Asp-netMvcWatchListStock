﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WatchListweb.Models
{
    public class Add
    {
        public int Id { get; set; }
        public string Name { get; set; }
        //public DateTime dateOfBuy { get; set; }
        public float FirstFund { get; set; }
        public int Number_of_days_of_deposit { get; set; }
        public int Profit { get; set; }
        public int Damage { get; set; }
        public int Cout_of_take_stock { get; set; }
        public float Expected_profit { get; set; }
        public float Expected_Damage { get; set; }
        public int persent_of_profit { get; set; }
        public int persent_of_damage { get; set; }
        public int The_number_of_days_left { get; set; }
        public string ImgPath { get; set; }
        public int Buy_price { get; set; }

    }
}