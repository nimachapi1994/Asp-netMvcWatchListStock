﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;

namespace WatchListweb.Models
{
    public class AddConfig:EntityTypeConfiguration<Add>
    {
        public AddConfig()
        {
            HasKey(x => x.Id);
            Property(x => x.Name).HasColumnAnnotation
                ("name", new IndexAnnotation(new IndexAttribute("x") { IsUnique = true }));
            //Property(x => x.dateOfBuy).HasColumnName("date_of_buy");
            Property(x => x.FirstFund);
            Property(x => x.Profit);
            Property(x => x.Damage);
            Property(x => x.Cout_of_take_stock).HasColumnName("count_of_take_stock");
            Property(x => x.Expected_profit);
            Property(x => x.Expected_Damage);
            Property(x => x.persent_of_profit);
            Property(x => x.persent_of_damage);
            Property(x => x.The_number_of_days_left);
            Property(x => x.ImgPath).HasColumnOrder(3).
                HasParameterName("Stock_picture").HasColumnName("picture_of_stock");

        }
    }
}