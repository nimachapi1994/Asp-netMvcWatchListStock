﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WatchListweb.Models;
using System.Data.Entity;
using System.Net.Http;
using System.Web.Helpers;

namespace WatchListweb.Controllers
{
    public class HomeController : Controller
    {
        model context = new model();
        public ActionResult Show_my_Stock()
        {
            System.Threading.Thread.Sleep(3000);

            return PartialView(context.Adds.ToList());
        }


        public ActionResult Getimg(HttpPostedFileBase image)
        {
            string path = null;
            string[] ExStr = { "image/jpg", "image/jpeg", "image/png" };
            byte[] b = null;
            b = new byte[image.ContentLength];
            image.InputStream.Read(b, 0, b.Length);
            WebImage img = new WebImage(b);
            if (image!=null && img.Height>1500 && img.Width>1200)
            {                           
                img.Resize(900,600);

                string pic = System.IO.Path.GetFileName(image.FileName);
                path = System.IO.Path.Combine(Server.MapPath("~/img/"), pic);
                img.Save(path);
                return RedirectToAction("Index");
            }
            if (!ExStr.Contains(image.ContentType))
            {
                return Content("this format is not match");

            }
            else
            {
                
                //string pic = System.IO.Path.GetFileName(image.FileName);
                //path = System.IO.Path.Combine(Server.MapPath("~/img/"), pic);
                //img.Save(path);
                //image.SaveAs(path);

                //foreach (var item in context.Adds.ToList())
                //{
                //    if (item.Id == (int)Session["id"])
                //    {
                //        item.ImgPath = path;
                //        context.SaveChanges();
                //    }
                //}
              
            }

            return RedirectToAction("Index");
        }
        public int InsertStock(Add a)
        {

            context.Adds.Add(new Add
            {
                Name = a.Name,
                Expected_Damage = a.Expected_Damage,
                Expected_profit = a.Expected_profit,
                Cout_of_take_stock = a.Cout_of_take_stock,
                Number_of_days_of_deposit = a.Number_of_days_of_deposit,
                FirstFund = a.Buy_price * a.Cout_of_take_stock,
                Buy_price = a.Buy_price,
                Profit = Convert.ToInt32(a.Expected_profit * a.Cout_of_take_stock),
                Damage = Convert.ToInt32(a.Expected_Damage * a.Cout_of_take_stock),


            });
            context.SaveChanges();
            string name = context.Adds.Where(x => x.Name == a.Name).Select(y => y.Name).ToString();
            string Stock_name = name;

            return 1;
        }
        //public ActionResult UploadImgView(int id,HttpPostedFileBase img)
        //{            
        //    return RedirectToAction("Getimg", new { Stock_id = id ,image=img});
        //}

        public ActionResult AddStock()
        {
            System.Threading.Thread.Sleep(1000);
            return PartialView();
        }
        public ActionResult Index()
        {

            //try
            //{
            //    var add = context.Adds.Add(new Add { Name ="nima" });
            //    context.Entry(add).State = EntityState.Added;
            //    context.SaveChanges();
            //    return View();
            //}
            //catch (Exception)
            //{
            //    return Content("is not work");
            //    throw;
            //}
            return View();
        }
    }
}