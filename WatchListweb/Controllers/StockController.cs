﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WatchListweb.Models;

namespace WatchListweb.Controllers
{
    public class stockController : ApiController
    {
        Models.model db = new Models.model();    
        public List<Add> Get()
        {
            return db.Adds.ToList();
        }
        public int Post(Add a)
        {
            db.Adds.Add(new Add
            {
                Name=a.Name,
                Damage=a.Damage,
                Profit=a.Profit,
                Cout_of_take_stock=a.Cout_of_take_stock,
                The_number_of_days_left=a.The_number_of_days_left,
                FirstFund=a.FirstFund
            });
            db.SaveChanges();
            return 1;
          
        }
    }
}
